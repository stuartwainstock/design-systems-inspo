# design_systems
Collection of Inspiration for Design Systems

A working list of organizations with interesting design systems that are either active or in the works.

## Orgs

| Name | Type | URL | Notes
| --- | --- | --- | --- |
| [Fabric Design]| Web | https://fabric-design.github.io/styleguide/ |
| [Nord Health]| Web | https://nordhealth.design/ |
| [Telia]| Storybook | https://teliads.netlify.app/|
| [Turva]| Web | https://www.duetds.com/?theme=turva |

## Books
| Name | Author | URL | Notes
| --- | --- | --- | --- |
| [Design System Foundations](https://designsystemfoundations.com/) | Andrew Couldwell | https://designsystemfoundations.com/ |
